<?php

namespace Drupal\mailyoo;
use Drupal\rest\LinkManager\LinkManager;
use Drupal\rest\LinkManager\TypeLinkManager;

/**
 * Class Rest.
 *
 * @package Drupal\mailyoo
 */
class Rest implements RestInterface {

  /**
   * Drupal\rest\LinkManager\LinkManager definition.
   *
   * @var \Drupal\rest\LinkManager\LinkManager
   */
  protected $restLinkManager;
  /**
   * Drupal\rest\LinkManager\TypeLinkManager definition.
   *
   * @var \Drupal\rest\LinkManager\TypeLinkManager
   */
  protected $restLinkManagerType;
  /**
   * Constructor.
   */
  public function __construct(LinkManager $rest_link_manager, TypeLinkManager $rest_link_manager_type) {
    $this->restLinkManager = $rest_link_manager;
    $this->restLinkManagerType = $rest_link_manager_type;
  }


  public getEmails($page){

  }

}
