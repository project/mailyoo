<?php

namespace Drupal\mailyoo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 * @author Saurabh Patel
 * @link url text
 * @category List Subscriber
 * @license license url license name
 * @package Drupal\mailyoo\Controller
 *
 */
class AdminController extends ControllerBase {

	/**
	 * List.
	 *
	 * @return array
	 *   Return Hello string.
	 */
	public function list() {
		return [
			'#type' => 'markup',
			'#markup' => $this->t('Implement method: list')
		];
	}

}
