<?php

namespace Drupal\mailyoo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MailyooBlockForm.
 *
 * @package Drupal\mailyoo\Form
 */
class MailyooForm extends ConfigFormBase {

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {
		return [
			'mailyoo.setting',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {
		return 'mailyoo_form';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		$config = $this->config('config.factory');

		$form['mailyoo_host'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('Mailyoo Host'),
			'#default_value' => $config->get('mailyoo_host'),
		);

		$form['mailyoo_key'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('API KEY/Username'),
			'#default_value' => $config->get('mailyoo_key'),
		);
		$form['mailyoo_secret'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('Secret/Password'),
			'#default_value' => $config->get('mailyoo_secret'),
		);
		$form['mailyoo_list_id'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('List Id'),
			'#default_value' => $config->get('mailyoo_list_id'),
		);
		$form['mailyoo_status'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('Status'),
			'#default_value' => $config->get('mailyoo_status'),
		); 
		$form['cms_type'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('CMS(PHPLIST/MAILYOO)'),
			'#default_value' => $config->get('cms_type'),
		);



		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		// $config = \Drupal::service('config.factory')->getEditable('mailyoo.Mailyoo');
		$config = $this->config('mailyoo.setting');
		$config->set('mailyoo_host', $form_state->getValue('mailyoo_host'));
		$config->set('mailyoo_key', $form_state->getValue('mailyoo_key'));
		$config->set('mailyoo_secret', $form_state->getValue('mailyoo_secret')); 
		$config->set('mailyoo_status', $form_state->getValue('mailyoo_status'));
		$config->set('mailyoo_list_id', $form_state->getValue('mailyoo_list_id'));
		$config->set('cms_type', $form_state->getValue('cms_type')); 
		$config->save();
		// $this->config('mailyoo.Mailyoo')->save();
		parent::submitForm($form, $form_state);
	}

}
