<?php

namespace Drupal\mailyoo\Form; 

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Class MailyooBlockForm.
 *
 * @package Drupal\mailyoo\Form
 */
class MailyooBlockForm extends FormBase {
 

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) { 

		    // Date.
		    $form['name'] = [
		      '#type' => 'textfield',
		      '#title' => $this->t('Name'), 
		      // '#description' => 'Your Name',
		    ]; 
		    // Email.
		    $form['email'] = [
		      '#type' => 'email',
		      '#title' => $this->t('Email'),
		      // '#description' => 'Enter your email',
		    ]; 

		    // Group submit handlers in an actions element with a key of "actions" so
		    // that it gets styled correctly, and so that other modules may add actions
		    // to the form.
		    $form['actions'] = [
		      '#type' => 'actions',
		    ]; 

		    // Add a submit button that handles the submission of the form.
		    $form['actions']['submit'] = [
		      '#type' => 'submit',
		      '#value' => $this->t('Subscribe'),
		      '#description' => $this->t('Submit, #type = submit'),
		    ];

		    return $form;
		  }

		  /**
		   * {@inheritdoc}
		   */
		  public function getFormId() {
		    return 'mailyoo_block_form';
		  }

		  /**
		   * {@inheritdoc}
		   */
		  public function submitForm(array &$form, FormStateInterface $form_state) { 
		    // $username = \Drupal::configFactory()->get('mailyoo.setting');

		    $config = \Drupal::config('system.performance');
		    var_dump( $config); die;
		    $username = \Drupal::config('mailyoo.setting');
		    // $password = \Drupal::config('mailyoo.setting')->get('mailyoo_secret');


		    var_dump($username  ); die;
		    $username = \Drupal::service('mailyoo.setting')->get('mailyoo_key'); 
		    $password = \Drupal::service('mailyoo.setting')->get('mailyoo_secret'); 
		    $mailyoo_host = \Drupal::service('mailyoo.setting')->get('mailyoo_host');
		    $listid = \Drupal::service('mailyoo.setting')->get('mailyoo_list_id');
		    $stauts = \Drupal::service('mailyoo.setting')->get('mailyoo_status'); 
		    if( $username ){ 
		    	$ch = curl_init();
				$curlConfig = array(
				    CURLOPT_URL            => $mailyoo_host.'email',
				    CURLOPT_POST           => true,
				    CURLOPT_RETURNTRANSFER => true,
				    CURLOPT_TIMEOUT  	   => 100,
				    CURLOPT_CONNECTTIMEOUT => 3,
				    CURLOPT_HEADER 		=> 1, 
				    CURLOPT_POSTFIELDS     => array(
				        'email' => $form['email']['#value'],
				        'name' => $form['name']['#value'] ,
				        'listid' =>$listid,
				        'stauts' => $stauts
				    )
				);
				curl_setopt_array($ch, $curlConfig); 
				curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password); 
				$result = curl_exec($ch);  
			    $message = $this->t('Thank you.');
			    drupal_set_message($message); 

		    }else{
		    	$message = $this->t('Please set config');
		    	drupal_set_message($message); 	
		    }
		    

		  }



}
