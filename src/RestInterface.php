<?php

namespace Drupal\mailyoo;

/**
 * Interface RestInterface.
 *
 * @package Drupal\mailyoo
 */
interface RestInterface {

/**
 * [getEmails description]
 * @return [type] [description]
 */
	public function getEmails($page);

}
