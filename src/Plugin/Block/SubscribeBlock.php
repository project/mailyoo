<?php

namespace Drupal\mailyoo\Plugin\Block;
use Drupal\Core\Block\BlockBase;

use Drupal\Core\Form\FormStateInterface;

/**
 * @Block(
 *   id = "subscribe",
 *   admin_label = @Translation("Subscribe to Mailyoo")
 * )
 */
class SubscribeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {  
  	return \Drupal::formBuilder()->getForm('Drupal\mailyoo\Form\MailyooBlockForm');  
  }




}
